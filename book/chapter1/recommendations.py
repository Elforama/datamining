from math import sqrt
from itertools import chain
import numpy as np

users = {"Angelica": {"Blues Traveler": 3.5, "Broken Bells": 2.0, "Norah Jones": 4.5, "Phoenix": 5.0,
                      "Slightly Stoopid": 1.5, "The Strokes": 2.5, "Vampire Weekend": 2.0},
         "Bill": {"Blues Traveler": 2.0, "Broken Bells": 3.5, "Deadmau5": 4.0, "Phoenix": 2.0, "Slightly Stoopid": 3.5,
                  "Vampire Weekend": 3.0},
         "Chan": {"Blues Traveler": 5.0, "Broken Bells": 1.0, "Deadmau5": 1.0, "Norah Jones": 3.0, "Phoenix": 5,
                  "Slightly Stoopid": 1.0},
         "Dan": {"Blues Traveler": 3.0, "Broken Bells": 4.0, "Deadmau5": 4.5, "Phoenix": 3.0, "Slightly Stoopid": 4.5,
                 "The Strokes": 4.0, "Vampire Weekend": 2.0},
         "Hailey": {"Broken Bells": 4.0, "Deadmau5": 1.0, "Norah Jones": 4.0, "The Strokes": 4.0,
                    "Vampire Weekend": 1.0},
         "Jordyn": {"Broken Bells": 4.5, "Deadmau5": 4.0, "Norah Jones": 5.0, "Phoenix": 5.0, "Slightly Stoopid": 4.5,
                    "The Strokes": 4.0, "Vampire Weekend": 4.0},
         "Sam": {"Blues Traveler": 5.0, "Broken Bells": 2.0, "Norah Jones": 3.0, "Phoenix": 5.0,
                 "Slightly Stoopid": 4.0, "The Strokes": 5.0},
         "Veronica": {"Blues Traveler": 3.0, "Norah Jones": 5.0, "Phoenix": 4.0, "Slightly Stoopid": 2.5,
                      "The Strokes": 3.0}
         }

def calculateDistance(user_ratings1: dict, user_ratings2: dict, r: int) -> int:
    distance = 0
    commonMovies = False
    for movie in user_ratings1:
        if movie in user_ratings2:
            distance += pow(abs(user_ratings1[movie] - user_ratings2[movie]), r)
            commonMovies = True

    if commonMovies:
        return pow(distance, 1 / r)
    else:
        return -1


def minkowski(username: str, users: dict, r: int) -> dict:
    distances = []

    for user in users:
        if user is username:
            continue
        else:
            distance = calculateDistance(users[username], users[user], r)
            distances.append((distance, user))

    distances.sort()
    return distances


def recommend(username: str, users: dict) -> list:
    nearest = minkowski(username, users, 2)[0][1]

    possibleRecommendations = users[nearest]
    moviesNotToRecommend = users[username]

    recommendations = []

    for movie in possibleRecommendations:
        if not movie in moviesNotToRecommend:
            recommendations.append((movie, possibleRecommendations[movie]))

    return sorted(recommendations,
                  key=lambda artistTuple: artistTuple[1],
                  reverse=True)

def pearsonCoefficient(user1: dict, user2: dict) -> float:

    sumXY = 0

    sumX = 0
    sumY = 0

    sumXSquared = 0
    sumYSquared = 0

    n = 0

    for key in user1:
        if key in user2:
            sumXY += user1[key] * user2[key]

            sumX += user1[key]
            sumY += user2[key]

            sumXSquared += user1[key] ** 2
            sumYSquared += user2[key] ** 2

            n += 1

    if n == 0:
        return 0

    denominator = sqrt(sumXSquared - sumX ** 2 / n) * sqrt(sumYSquared - sumY ** 2 / n)

    if denominator == 0:
        return 0

    numerator = sumXY - (sumX * sumY) / n

    return numerator / denominator

def cosineSimilarity(userRatings1: dict, userRatings2: dict) -> float:

    xVectorLen = 0.0
    yVectorLen = 0.0
    xVector = []
    yVector = []

    filter = []

    for rating in chain(userRatings1, userRatings2):
        if rating in filter:
            continue
        filter.append(rating)

        xVectorLen += userRatings1.get(rating, 0) ** 2
        xVector.append(userRatings1.get(rating, 0))
        yVectorLen += userRatings2.get(rating, 0) ** 2
        yVector.append(userRatings2.get(rating, 0))

    xVectorLen = sqrt(xVectorLen)
    yVectorLen = sqrt(yVectorLen)

    numerator = np.dot(xVector, yVector)
    denominator = xVectorLen * yVectorLen

    return numerator / denominator

def run():
    print(cosineSimilarity(users["Angelica"], users["Veronica"]))
    #print(pearsonCoefficient(users["Angelica"], users["Jordyn"]))
